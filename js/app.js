const itemCtrl = require('./itemCtrl');
const uiCtrl = require('./uiCtrl');
const storageCtrl = require('./storageCtrl');

const app = (function(itemCtrl, uiCtrl, storageCtrl){
    const loadEventListeners = function(){
        const uiSelectors = uiCtrl.getSelectors();
        document.querySelector(uiSelectors.addBtn).addEventListener('click', itemAddSubmit);
        document.querySelector(uiSelectors.itemList).addEventListener('click', itemEditClick);
        document.querySelector(uiSelectors.updateBtn).addEventListener('click', itemUpdateSubmit);
        document.querySelector(uiSelectors.backBtn).addEventListener('click', uiCtrl.clearEditState);
        document.querySelector(uiSelectors.deleteBtn).addEventListener('click', itemDeleteSubmit);
        document.querySelector(uiSelectors.clearBtn).addEventListener('click', clearAllItemsClick);
        document.addEventListener('keypress', function(e){
            if(e.keyCode === 13 || e.which === 13){
                e.preventDefault();
                return false;
            }
        });
    }

    const itemAddSubmit = function(e){
        const input = uiCtrl.getItemInput();
        if(input.name !== '' && input.calories !== ''){
            const item = itemCtrl.addItem(input.name, input.calories);
            uiCtrl.addListItem(item);            
            storageCtrl.storeItem(item);
            uiCtrl.updateTotalCalories(itemCtrl.getTotalCalories());
            uiCtrl.clearInput();
        }
        e.preventDefault();
    }
    const itemEditClick = function(e){
        if(e.target.classList.contains('edit-item')){
            const id = parseInt(e.target.parentNode.parentNode.id.split('-')[1]);
            itemCtrl.setCurrentItem(itemCtrl.getItemById(id));
            uiCtrl.addItemToForm(itemCtrl.getCurrentItem());
        }
        e.preventDefault();
    }

    const itemUpdateSubmit = function(e){
        const input = uiCtrl.getItemInput();
        const updateItem = itemCtrl.updateItem(input.name, input.calories);
        uiCtrl.updateListItem(updateItem);
        storageCtrl.updateItem(updateItem);
        uiCtrl.clearEditState();
        e.preventDefault();
    }

    const itemDeleteSubmit = function(e){
        const item = itemCtrl.getCurrentItem();
        itemCtrl.deleteItem(item.id);
        storageCtrl.deleteItem(item.id);
        uiCtrl.deleteItem(item.id);
        uiCtrl.clearEditState();
        e.preventDefault();
    }

    const clearAllItemsClick = function(e){
        itemCtrl.deleteAllItems();
        storageCtrl.clearItems();
        uiCtrl.deleteAllItems();
        uiCtrl.updateTotalCalories(itemCtrl.getTotalCalories());
        e.preventDefault();
    }

    return {
        init: function(){
            uiCtrl.clearEditState();
            const items = itemCtrl.getItems();
            items.length === 0 ? uiCtrl.hideList():uiCtrl.populateItemList(items);
            uiCtrl.updateTotalCalories(itemCtrl.getTotalCalories());
            loadEventListeners();
        }
    }

})(itemCtrl, uiCtrl, storageCtrl);
module.exports = app;