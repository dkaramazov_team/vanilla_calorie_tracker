const storageCtrl = (function(){
    return {
        storeItem: function(item){
            let items;
            if(localStorage.getItem('items') === null){
                items = [];
            } else {
                items = JSON.parse(localStorage.getItem('items'));
                items.push(item);
            }
            localStorage.setItem('items', JSON.stringify(items));
        },
        getItems: function(){
            return localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')):[];
        },
        deleteItem: function(id){
            const items = storageCtrl.getItems();
            const index = items.findIndex(function(item){
                return item.id === id;
            });
            items.splice(index, 1);
            localStorage.setItem('items', JSON.stringify(items));
        },
        updateItem: function(updateItem){
            const items = storageCtrl.getItems();
            const index = items.findIndex(function(item){
                return item.id === updateItem.id;
            });
            items[index].name = updateItem.name;
            items[index].calories = updateItem.calories;
            localStorage.setItem('items', JSON.stringify(items));
        },
        clearItems: function(){
            localStorage.setItem('items', JSON.stringify([]));
        }
    }
})();
module.exports = storageCtrl;