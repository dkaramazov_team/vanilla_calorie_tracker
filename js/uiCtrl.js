const uiCtrl = (function(){
    const uiSelectors = {
        itemList: '#item-list',
        addBtn: '.add-btn',
        itemNameInput: '#item-name',
        itemCaloriesInput: '#item-calories',
        totalCalories: '.total-calories',
        updateBtn: '.update-btn',
        backBtn: '.back-btn',
        deleteBtn: '.delete-btn',
        clearBtn: '.clear-btn'
    }
    return {
        populateItemList: function(items){
            items.forEach(function(item){
                document.querySelector(uiSelectors.itemList).innerHTML += `
                <li id="item-${item.id}" class="collection-item">
                    <strong>${item.name}: </strong> 
                    <em>${item.calories} Calories</em>
                    <a href="#" class="secondary-content">
                        <i class="edit-item fa fa-pencil"></i>
                    </a>
                </li>`
            });
        },
        getSelectors: function(){
            return uiSelectors;
        },
        getItemInput: function(){
            return {
                name: document.querySelector(uiSelectors.itemNameInput).value,
                calories: document.querySelector(uiSelectors.itemCaloriesInput).value
            }
        },
        addListItem: function(item){
            document.querySelector(uiSelectors.itemList).style.display = 'block';
            const li = document.createElement('li');
            li.className = 'collection-item';
            li.id = `item-${item.id}`;
            li.innerHTML = `<strong>${item.name}: </strong> 
            <em>${item.calories} Calories</em>
            <a href="#" class="secondary-content">
                <i class="edit-item fa fa-pencil"></i>
            </a>`
            document.querySelector(uiSelectors.itemList).insertAdjacentElement('beforeend', li);
        },
        updateListItem: function(item){
            const element = document.querySelector(`#item-${item.id}`);
            element.innerHTML = `<strong>${item.name}: </strong> 
            <em>${item.calories} Calories</em>
            <a href="#" class="secondary-content">
                <i class="edit-item fa fa-pencil"></i>
            </a>`;
        },
        deleteItem: function(id){
            document.querySelector(`#item-${id}`).remove();
        },
        clearInput: function(){
            document.querySelector(uiSelectors.itemNameInput).value = '';
            document.querySelector(uiSelectors.itemCaloriesInput).value = '';
        },
        hideList: function(){
            document.querySelector(uiSelectors.itemList).style.display = 'none';
        },
        updateTotalCalories: function(calories){
            document.querySelector(uiSelectors.totalCalories).textContent = calories;
        },
        clearEditState:  function(){
            document.querySelector(uiSelectors.updateBtn).style.display = 'none';
            document.querySelector(uiSelectors.deleteBtn).style.display = 'none';
            document.querySelector(uiSelectors.backBtn).style.display = 'none';
            document.querySelector(uiSelectors.addBtn).style.display = 'inline';
            uiCtrl.clearInput();
        },
        addItemToForm: function(item){
            document.querySelector(uiSelectors.itemNameInput).value = item.name;
            document.querySelector(uiSelectors.itemCaloriesInput).value = item.calories;
            uiCtrl.showEditState();
        },
        showEditState: function(){
            document.querySelector(uiSelectors.updateBtn).style.display = 'inline';
            document.querySelector(uiSelectors.deleteBtn).style.display = 'inline';
            document.querySelector(uiSelectors.backBtn).style.display = 'inline';
            document.querySelector(uiSelectors.addBtn).style.display = 'none';
        },
        deleteAllItems: function(){
            const nodes = Array.from(document.querySelector(uiSelectors.itemList).childNodes);
            nodes.forEach(function(node){
                node.remove();
            });
        }
    }
})();
module.exports = uiCtrl;