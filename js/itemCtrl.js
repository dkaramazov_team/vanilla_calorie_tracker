const storageCtrl = require('./storageCtrl');
const itemCtrl = (function(storageCtrl){

    const Item = function(id, name, calories){
        this.id = id;
        this.name = name;
        this.calories = calories;
    }

    const data = {
        items: storageCtrl.getItems(),
        currentItem: null,
        totalCalories: 0
    }

    return {
        getItems: function(){
            return data.items;
        },
        getItemById: function(id){
            return data.items.find(function(item){
                return item.id === id;
            });
        },
        logData: function(){
            return data;
        },
        addItem: function(name, calories){
            const item = new Item(data.items.length>0?data.items.length:0, name, parseInt(calories));
            data.items.push(item);
            return item;
        },
        setCurrentItem: function(item){
            data.currentItem = item;
        },
        getCurrentItem: function(){
            return data.currentItem;
        },
        deleteAllItems: function(){
            data.items = [];
        },
        updateItem: function(name, calories){
            const current = itemCtrl.getCurrentItem();
            current.name = name;
            current.calories = calories;
            return current;
        },
        deleteItem: function(id){
            const index = data.items.findIndex(function(item){
                return item.id === id;
            });
            data.items.splice(index, 1);
        },
        getTotalCalories: function(){
            return data.items.reduce(function(acc, item){
                return acc + item.calories;
            }, 0);
        }
    }

})(storageCtrl);
module.exports = itemCtrl;